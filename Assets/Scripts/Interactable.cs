﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Interactable : MonoBehaviour
{


    // Virtual method 
    public virtual void objectInteract()
    {
        //Debug.Log("GenericLogic"); //log it if debug
        // Generic interactive stuff
        //GetComponent(<InteractiveObject>).objectInteract()
    }

    
}
