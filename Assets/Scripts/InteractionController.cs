﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class InteractionController : MonoBehaviour
{
    public bool debug = true;
    public float interactionDistance = 2;
    public bool tmpCheckedForObject = true;
    public Texture GrabStateCrosshair = null;
    public Texture LockedStateTexture = null;
    //private GameObject[] interactableObjects;
    private GameObject lookingAt;
    private bool displayInteractTexture;
    private bool displayLockedTexture;
    // Start is called before the first frame update
    void OnGUI()
    {
        if (!GrabStateCrosshair || !LockedStateTexture)
        {
            Debug.LogError("Assign a Texture in the inspector.");
            return;
        }
        GameObject lookingAt = getLookingAtIfInteractable();
        if (lookingAt != null)
        {
            try
            {
                SlidingDoor slidingDoor = lookingAt.GetComponent<SlidingDoor>();
                if (slidingDoor.displayInteract())
                    GUI.DrawTexture(new Rect((Screen.width / 2) - 16, (Screen.height / 2) - 16, 32, 32), GrabStateCrosshair, ScaleMode.StretchToFill, true, 10.0F);
                if (slidingDoor.displayLock())
                    GUI.DrawTexture(new Rect((Screen.width / 2) - 16, (Screen.height / 2) - 16, 32, 32), LockedStateTexture, ScaleMode.StretchToFill, true, 10.0F);
            }
            catch (Exception e)
            {
                GUI.DrawTexture(new Rect((Screen.width / 2) - 16, (Screen.height / 2) - 16, 32, 32), GrabStateCrosshair, ScaleMode.StretchToFill, true, 10.0F);
            }
            
        }


            
    }

    void Start()
    {
        //interactableObjects = GameObject.FindGameObjectsWithTag("Interactable");
    }

    // Update is called once per frame
    void Update()
    {
        //debuging lets you see the ray cast up to unit of 10
        if (debug)
        {
            Vector3 forward = transform.TransformDirection(Vector3.forward) * 10;
            Debug.DrawRay(transform.position, forward, Color.green);
        }
        RaycastHit hit;
        //checks to see if the raycast hits
        if (Physics.Raycast(transform.position, transform.TransformDirection(Vector3.forward) * 10, out hit))
        {
            lookingAt = getLookingAtIfInteractable();
            //if (debug)
            //    Debug.Log(lookingAt.name);
            if (lookingAt != null) // checks if object is tagged as interactable
            {
                    displayInteractTexture = true;
                    if (debug)
                        Debug.Log("Object Interactable at distance: " + hit.distance); //log it if debug
                    if (vp_Input.GetButton("Interact"))
                        lookingAt.GetComponent<Interactable>().objectInteract();
            }
            else
            {
                displayInteractTexture = false;
            }
        }
    }

    public GameObject getLookingAtIfInteractable()
    {
        RaycastHit hit;
        //checks to see if the raycast hits
        if (Physics.Raycast(transform.position, transform.TransformDirection(Vector3.forward) * 10, out hit))
        {
            lookingAt = hit.collider.gameObject; //gets the object the raycast hit
            //if (debug)
            //    Debug.Log(lookingAt.name);
            if (lookingAt.tag.Equals("Interactable")) // checks if object is tagged as interactable
            {
                if (hit.distance < interactionDistance)
                {
                    if (debug)
                        Debug.Log("Object Interactable at distance: " + hit.distance); //log it if debug
                    return lookingAt;
                }
            }
        }
        return null;
    }
}
