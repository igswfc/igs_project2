﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class I_Light : MonoBehaviour
{
    public GameObject boolStateControllerObject = null;         //lock controller object
    //=========private public seperator=========
    private Light light;

    // Start is called before the first frame update
    void Start()
    {
        light = gameObject.GetComponent<Light>();
        light.enabled = boolStateControllerObject.GetComponent<BSCMono>().getState();
    }

    // Update is called once per frame
    void Update()
    {
        if (light.enabled != boolStateControllerObject.GetComponent<BSCMono>().getState())
            light.enabled = !light.enabled;
    }
   
}
