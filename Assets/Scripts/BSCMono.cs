﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BSCMono : MonoBehaviour
{
    private bool state = false; //default false
    public bool startTrue = false; 

    // Start is called before the first frame update
    public BSCMono()
    {
        state = startTrue;
    }

    public bool getState()
    {
        return state;
    }

    public void toggleState()
    {
        state = !state;
        //Debug.Log("BoolStateController: SwapState");
    }
}
