﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class LightFlickering : MonoBehaviour

    
{
    public float offOnRatio = (float)0.8;
    public int rangeLB = 10;
    public int rangeUB = 1000;
    public float scale = 1;
    public int width = 100;
    private Light light = null;
    private float counterTime = 0;
    private int counter = 0;
    private float[] noise = null;
    // Start is called before the first frame update
    void Start()
    {
        light = gameObject.GetComponent<Light>();
        System.Random random = new System.Random();
        noise = new float[width];
        for (int i = 0; i < width; i++)
        {
            noise[i] = (float) (random.Next(rangeLB, rangeUB) /(float)1000);
        }
        
    }

    // Update is called once per frame
    void Update()
    {
        counterTime += Time.deltaTime;
        
        if ((!light.enabled && counterTime > scale * offOnRatio * noise[counter]) || (counterTime > scale * (2-offOnRatio) * noise[counter]))
        {
            counterTime = 0;
            light.enabled = !(light.enabled);
            counter++;
            if (counter >= width-1)
                counter = 0;
    }
        }

    
}
