﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OnCollideUnfreeze : MonoBehaviour
{
    public static int size;
    public GameObject[] rb = new GameObject[size];
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Player")
        {
            foreach (GameObject obj in rb)
            {
                obj.GetComponent<Rigidbody>().constraints = RigidbodyConstraints.None;
            }
        }
    }
}
