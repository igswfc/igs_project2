﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class BtnManager : MonoBehaviour
{
    public void pressPlayButton(string newGameLevel)
    {
        SceneManager.LoadScene(newGameLevel, LoadSceneMode.Single);
        Debug.Log("Play Button");
    }

    public void pressExitButton()
    {
        Application.Quit();
        Debug.Log("Exit Button");
    }
}
