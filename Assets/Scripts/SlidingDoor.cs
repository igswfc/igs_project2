﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SlidingDoor : Interactable
{
    private Animator animator;                                  //animator componeont
    private bool controllerState = false;
    //=========private public seperator=========
    public bool requierUnlock = false;                          //determins if door uses lock
    public bool keyController = false;                          //use key controller or another object
    public int keyNumber = -1;
    public GameObject boolStateControllerObject = null;         //lock controller object
    private KeyController keyControllerObject = null;               // key controller object

    public override void objectInteract()
    {
        if (requierUnlock) //grabs if door is unlocked if locks are being used
        {
            if (!keyController)
                controllerState = boolStateControllerObject.GetComponent<BSCMono>().getState();
            else if (keyController)
                controllerState = keyControllerObject.getKeyState(keyNumber);
        }
        else  //else unlocks door
        {
            controllerState = true;
        }
        if (controllerState) //if ulocked
        {
            animator = GetComponent<Animator>();
            if (animator != null)
            {
                Debug.Log("SwapState recived"); //log it if debug
                animator.SetTrigger("SwapState");
            }
            else
            {
                Debug.Log("No Animator attached to object");
            }
        }
        //base interaction class
        base.objectInteract();

    }



    public bool displayInteract()
    {
        update();
        return controllerState;

    }
    public bool displayLock()
    {
        update();
        return !controllerState;
    }

    private void update()
    {
        if (requierUnlock) //grabs if door is unlocked if locks are being used
        {
            if (!keyController)
                controllerState = boolStateControllerObject.GetComponent<BSCMono>().getState();
            else if (keyController)
                controllerState = keyControllerObject.getKeyState(keyNumber);
        }
        else  //else unlocks door
        {
            controllerState = true;
        }
    }

    void Start()
    {
        if (keyController)
            foreach (GameObject obj in GameObject.FindGameObjectsWithTag("KeyController"))
                keyControllerObject = obj.GetComponent<KeyController>();
    }
}