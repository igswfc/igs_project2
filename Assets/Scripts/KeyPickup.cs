﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KeyPickup : MonoBehaviour
{

    private KeyController keyControllerObject = null;
    public int keyNumber = -1;

    void OnCollisionEnter(Collision col)
    {
        if (col.gameObject.tag == "Player")
        {
            Debug.Log("Collided with player");
            keyControllerObject.setKeyState(keyNumber, true);
            Destroy(this);
        }
    }
    void OnTriggerEnter(Collider other)
    {
        //Debug.Log("Collided with player");
        if (other.gameObject.tag == "Player")
        {
            Debug.Log("Collided with player");
            keyControllerObject.setKeyState(keyNumber, true);
            Destroy(gameObject);
        }
    }
    // Start is called before the first frame update
    void Start()
    {
        foreach (GameObject obj in GameObject.FindGameObjectsWithTag("KeyController"))
            keyControllerObject = obj.GetComponent<KeyController>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
