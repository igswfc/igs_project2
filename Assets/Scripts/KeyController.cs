﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KeyController : MonoBehaviour
{

    public int keyCount = 2;
    private BoolStateController[] key;

    public bool getKeyState(int i)
    {
        return key[i].getState();
    }

    public void toggleKeyState(int i)
    {
        key[i].toggleState();
    }

    public void setKeyState(int i, bool b)
    {
        if (key[i].getState() != b)
            key[i].toggleState();
    }
    // Start is called before the first frame update
    void Start()
    {
        key = new BoolStateController[keyCount];
        for (int i = 0; i< key.Length; i++)
        {
            key[i] = new BoolStateController();
            
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
