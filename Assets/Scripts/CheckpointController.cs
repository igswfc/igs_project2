﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CheckpointController : MonoBehaviour
{
    public GameObject playerObject;
    public GameObject respawnObject;

    void OnTriggerEnter(Collider other)
    {
        Debug.Log("Collided with player");
        respawnObject.transform.position = playerObject.transform.position;
        respawnObject.transform.rotation = playerObject.transform.rotation;
        Destroy(gameObject);
    }

}
