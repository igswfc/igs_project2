﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ButtonScript : Interactable
{
    private Animator animator;
    private bool currentlyEnabled = false;
    private bool init = false;
    public GameObject boolStateControllerObject = null;         //lock controller object
    public bool startEnabled = false;
    public override void objectInteract()
    {
        if (!init)
        {
            currentlyEnabled = startEnabled;
            animator = GetComponent<Animator>();
            init = true;
        }
        boolStateControllerObject.GetComponent<BSCMono>().toggleState();
        Debug.Log("SwapState recived");
        currentlyEnabled = !currentlyEnabled;
        animator.SetTrigger("ToggleState");
    }
}
