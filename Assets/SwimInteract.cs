﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
///     NOTE: Due to the Weapon Camera forcing only the Weapon Culling Mask, I commented
///     out line 250 of the vp_FPCamera script so that it would allow me to use the 
///     masks that I want (Weapon and Transparent &lt;-Underwater).
/// </summary>

public class SwimInteract : vp_Interactable
{
    public string SwimStateName = "Swim";

    protected override void Start()
    {
        //InteractType == vp_InteractType.Trigger;
        InteractType = vp_InteractType.Trigger;
        base.Start();
    }

    public override bool TryInteract(vp_PlayerEventHandler player)
    {
        if (m_Player == null)
            m_Player = player;

        m_Player.SetState(SwimStateName, true);
        return base.TryInteract(player);
    }

    public void OnTriggerExit(Collider other)
    {
        vp_PlayerEventHandler player = other.transform.root.GetComponent<vp_PlayerEventHandler>();
        if (player == null)
            return;

        player.SetState(SwimStateName, false);
    }
}